const express = require("express");
const app = express()
const bodyParser = require("body-parser");
const cors = require('cors');
const fs = require('fs');
const cookieParser = require("cookie-parser");
const session = require("express-session");



/*io.on("connection", socket => {
  userIds[socket.id] = currentUserId++;
  messageHandler.handleMessage(socket, userIds);
});*/



app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cors());

app.use(cookieParser());
app.use(
    session({
        key: "userId",
        secret: "subscribe",
        resave: false,
        saveUninitialized: false,
        cookie:{
            expires: 60 * 60 *24
        },
    })
);
const routes = require('../routes/routes.js')(app, fs);

app.use('/', require('./controllers/greeting.controller'));
app.use('/', require('./controllers/resetPassword'));

