  let currentMessageId = 1;

function createMessage(userId, messageText,pdp) {
  return {
    _id: currentMessageId++,
    text: messageText,
    createdAt: new Date(),
    user: {
      _id: userId,
      name: "Test User",
      avatar: (pdp =="")? 'https://www.blog-nouvelles-technologies.fr/wp-content/uploads/2017/12/detective-avatar-icon-01-.jpg':pdp
    }
  };
}

function handleMessage(socket, userIds,room) {
  socket.on("message", (messageText, avatar) => {
    const userId = userIds[socket.id];
    const message = createMessage(userId, messageText,avatar);
    console.log('titi')
    console.log(socket.rooms)
    console.log(message)
    socket.to(socket.rooms).emit("message", message);
  });
}

module.exports = { handleMessage };