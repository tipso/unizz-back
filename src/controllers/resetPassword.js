const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const router = express.Router();
const nodemailer = require("nodemailer");
const { text } = require('body-parser');
const users = require("../../data/users.json");
const fs = require('fs') ;
const bcrypt = require('bcrypt');

const resetPass = (req, res) =>{
    const requestedEmail = req.body.email;
    const fs = require('fs');
    fs.readFile("./data/users.json", 'utf8', (err, jsonString) => {
        if (err) {
            console.log("Error reading file :", err)
            return
        }
        try {
            const fileUsers = JSON.parse(jsonString);
            const tab = Object.keys(fileUsers);
            const foundEmail = tab.find(userMail => userMail === requestedEmail);
            if (foundEmail && users[foundEmail].email === requestedEmail) {
                delete users[foundEmail].password; 
                var newPass = Math.random().toString(36).slice(-7);
                //Math.random() génére un nombre x tel que 0<x<1
                //toString met le nombre généré en base 36 
                //slice(-7) prend les ses 7 dernier cararctères alphanumérique de ce nombre
                console.log(newPass) ;

                bcrypt.hash(newPass, 10, (err, passwordHash) => {
                    if (err) {
                        throw err
                    } else {
                        
                        console.log(passwordHash);
                    }
                    users[foundEmail].password = passwordHash;
                    fileUsers[requestedEmail] = users[foundEmail];
                    const jsonStringW = JSON.stringify(fileUsers, null, 2)
                    fs.writeFileSync("./data/users.json", jsonStringW, (err) =>{ 
                        if(err){
                            console.log(err)
                        }
                        else{
                            console.log('Successfully wrote file')
                        }
                    });
                })
                

                sendResetPass(foundEmail,newPass);
                sendResetPass(foundEmail).catch(err =>{
                    console.error(err.message);
                    process.exit(1); 
                });
                res.sendStatus(204);
            }
            else{
                res.sendStatus(404)
            }

    }catch(err) {
            console.log('Error parsing JSON string:', err)
        }
    })
}

async function sendResetPass(email, passNoHash){
    const mailTransport= nodemailer.createTransport({
        service :"Gmail", //"SendPulse", no need to set host or port etc     
        port: 587,
        secure: false,
        auth : {
            user: "unizz.service@gmail.com",  
            pass: "xxxxxx"
        }
    })
    const message = await mailTransport.sendMail({
        from: '"UNIZZ"<unizz.service@gmail.com>',
        to: email,
        subject: "reset pass-word",
        text: "Bonjour Voici votre nouveau mot de passe: "
        + passNoHash +
        " Bonne journée :) ",
        //html: <p><p>Bonjour</p>
            //<p>Voici votre nouveau mot de passe {{users[foundEmail].password}}</p> 
            //<p>Bonne journée</p>
        //</p>
    })
    console.log('Message sent successfully!');
    console.log(nodemailer.getTestMessageUrl(message));
}

router.post('/recover_password', resetPass)
module.exports = router;  