const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const router = express.Router();
const greetingService = require('../services/greeting.service');
const donnee = require("../../data/users.json");
const userRoutes = require('../../routes/users');
const fs = require('fs');





const sayHello = (req, res, next) => {
    
    const message = greetingService.sayHello();
    console.log('Ceci est un affichage côté serveur:', message);
    /* 
       Pour avoir le réel retour vous devez envoyer une requete GET sur la route http://localhost:3000/hello
        Ouvrez un second terminal tandis que votre serveur tourne sur un autre terminal et lancez la commande curl suivante: 
        curl 'http://localhost:3000/hello' 
        par défaut, curl fait un GET si on ne précise pas la méthode, voir avec le manuel: man curl
        Donc c'est pareil que: curl 'http://localhost:3000/hello' -X GET 
        Par contre si vous remplacez le GET par PUT par exemple, ca n'enverra rien car il n'y a pas de route configuré sur la méthode PUT sur l'endoint http://localhost:3000/hello
        => vous devriez recevoir le message 'Coucou!'. 
        Bravo! vous venez de faire votre première requette HTTP sur votre premier server !
    */
    res.send(message);
};
const sendPrenomAge = (req, res, next) => {
    const message = greetingService.prenomAge();
    res.send(JSON.stringify(message))
};


router.get('/hello', sendPrenomAge);

//fonction permettant de lire un fichier Json
function jsonReader(filePath, cb) {
    fs.readFile(filePath, (err, fileData) => {
      if (err) {
        return cb && cb(err);
      }
      try {
        const object = JSON.parse(fileData);
        return cb && cb(null, object);
      } catch (err) {
        return cb && cb(err);
      }
    });
}







module.exports = router;