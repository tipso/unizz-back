const donnee = require("../data/users_att.json");
const bcrypt = require('bcrypt');
const verifyJWT = require('./verifyToken');
const nodemailer = require("nodemailer");
const jwt = require('jsonwebtoken');



const userRoutes = (app, fs) => {
  //var keyPrivateToTalk
  const server = require("http").createServer(app);
  const io = require("socket.io")(server)

  


  const messageHandler = require("../src/message.handler");

  let currentUserId = 2;
  const userIds = {}
  /*io.on('subscribe', function(room) {
    console.log('joining room', room);
    socket.join(room);
});

io.on('send message', function(data) {
    console.log('sending room post', data.room);
    socket.broadcast.to(data.room).emit('conversation private post', {
        message: data.message
    });
});*/
  
  server.listen(3000, () => console.log("App listening on port 3000"));
//server.listen(3000, () => console.log("App listening on port 3000"));
    //Déclarations de constantes utiles pour ne pas répéter du code lors des app.requete
    const dataPath = './data/users.json';
    const dataPathBis='./data/users_att.json'
   

    const readFile = (
      callback,
      returnJson = false,
      filePath = dataPath,
      encoding = 'utf8'
    ) => {
      fs.readFile(filePath, encoding, (err, data) => {
        if (err) {
          throw err;
        }
  
        callback(returnJson ? JSON.parse(data) : data);
      });
    };

    const readFileBis = (
      callback,
      returnJson = false,
      filePath = './data/users_att.json',
      encoding = 'utf8'
    ) => {
      fs.readFile(filePath, encoding, (err, data) => {
        if (err) {
          throw err;
        }
  
        callback(returnJson ? JSON.parse(data) : data);
      });
    };
  
    const writeFile = (
      fileData,
      callback,
      filePath = dataPath,
      encoding = 'utf8'
    ) => {
      fs.writeFile(filePath, fileData, encoding, err => {
        if (err) {
          throw err;
        }
  
        callback();
      });
    };

    const writeFileBis = (
      fileData,
      callback,
      filePath = './data/users_att.json',
      encoding = 'utf8'
    ) => {
      fs.writeFile(filePath, fileData, encoding, err => {
        if (err) {
          throw err;
        }
  
        callback();
      });
    };
  
    function likeNumbersReset() {
      
      let reset = new Date();
      reset.setHours(24, 0, 0, 0);
      let t = reset.getTime() - Date.now();

      setTimeout(function() {
        readFile(data => {
          tabKeys = Object.keys(data)
          tabKeys.forEach(element => {
            data[element].likeNumbers = 0
          })
          writeFile(JSON.stringify(data, null, 2), () => {});
        }, true);
        likeNumbersReset();
      }, t);
  }
  likeNumbersReset();

    //Route get user pour récupérer les données d'un utilisateur
    app.get('/users',(req, res) => {
      readFile(data => {
        res.send(data);
      }, true);
    });

    app.get('/users/:id',(req, res) => {
      readFile(data => {
        const key = req.params['id'];
        let likeInNotif = data[key].likeInNotif;
        res.send(likeInNotif);
      }, true);
    }); 

    app.get('/users1/:id',(req, res) => {
      readFile(data => {
        const key = req.params['id'];
        let firstname = data[key].firstname;
        res.send(firstname);
      }, true);
    });

    


    //Route  put users/:id pour modifier l'utilisateur de clé "id"
    app.put('/users/:id', (req, res) => {
        readFile(data => {
          const userId = req.params['id'];
          data[userId] = req.body;
      
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send(`users id:${userId} updated`);
          });
        }, true);
        
    });

    //Route post /register pour créer un nouvel utilisateur si il n'existe pas, sinon envoie d'une requete 409
    app.post('/register', (req, res) => {
      //Bycrypt fonction asynchrone permettant de crypter le mot de passe 
      bcrypt.hash(req.body.password,10,(err,hash) => {
        if (err){
          return res.status(500).json({
            error: err
          });
        }
        let read =fs.readFileSync(dataPath,'utf8');
        tabKeys=Object.keys(JSON.parse(read));
        objValues=JSON.parse(read);
        const email = req.body.mail;
        user=tabKeys.find( elemTableau=> elemTableau===email);
        if(user===undefined){
          objValues[email] = {
            name:req.body.name,
            firstname:req.body.firstname,
            birthdate:req.body.birthdate,
            gender:req.body.gender,
            status:req.body.status,
            pseudo:req.body.pseudo,
            password:hash,
            email:email,
            personnalities:req.body.personnalities,
            likeIn: [],
            likeInNotif:[],
            likeOut: [],
            checkIn: [],
            checkOut: [],
            matchLike: [],
            matchCheck: [],
            masqued:[],
            likeNumbers: 0,           
          }
          
          writeFile(JSON.stringify(objValues, null, 2), () => {
            res.status(200).json({
              messsage:'Users registered'
            });
          });
          readFileBis(data => {
            const key = req.body.mail;
            data[key] = {
              pdp:[],
              picture:[],
              bio : "",
              recherche :"value1",
              passions : [],
              cours : "",
              musique :""
            };
        
            writeFileBis(JSON.stringify(data, null, 2), () => {
              res.status(200).send('new user added');
            });
          }, true);
    
        }else{
          return res.status(409).json({
            message: 'Mail is already registered!'
          });
        }
      });
    });

  

    app.get('/isauth', verifyJWT, (req, res) =>{
      res.send("Vous êtes authentifié Bravo!");
    });



    
    //Route post /login pour se connecter à l'application si on est déja inscrit
    app.post('/login', (req, res, next) =>{
      let read =fs.readFileSync(dataPath,'utf8')
      tabValues=Object.values(JSON.parse(read));

      let mail = req.body.mail;
      let password = req.body.password;
      user=tabValues.find( elemTableau=> elemTableau.email===mail);
    
      if(user===undefined){
          res.status(401).json({
          message: 'Auth failed!'
        });
      }
      else{
        bcrypt.compare(password,user.password,(err,result) =>{
          if(err){
            res.status(500).json({
              message:'Auth failed 2'
            });
          }
          if(result){

            const token = jwt.sign({ username: user.email}, "accessTokenSecret");
            req.session.usr = user;
            //res.headers('auth-token',token).send(token);
            res.header('x-access-token',token).status(200).json({
              auth : true,
              userId:user.email,
              message:'Auth succefull',
              token: token,
              result: user,
              
            });
          }else{
            res.status(401).json({
              message:'Auth failed 3'
            });
          }
        });
      }
    });

    
    app.post('/updatePassword/:id', (req, res) =>{
      let users =JSON.parse(fs.readFileSync(dataPath,'utf8'));
      const key = req.params['id'];
      let password = req.body.password;
      let  newPassword = req.body.newPassword
      let newConfirmPassword=req.body.newConfirmPassword
      if(newConfirmPassword===newPassword){
        bcrypt.compare(password,users[key].password,(err,result) =>{
          if(err){
            res.status(500).json({
              message:'Auth failed 2'
            });
          }
          if(result){
            bcrypt.hash(newPassword,10,(err,hash) => {
              if (err){
                return res.status(500).json({
                  error: err
                })
              }else{
                users[key].password=hash
                writeFile(JSON.stringify(users, null, 2), () => {
                  res.status(200).json({
                   messsage:'change registered'
                  });
                })
                sendResetPass(key)
              }
            })
          }else{
            res.status(401).json({
              message:'Mot de passe incorrect'
            });
          }
        })
      }else{
        res.status(401).json({
            message:'Mot de passe incorrect1'
          })
        }
    })

      

    

    app.post('/showFirsnameAge/:id', (req, res) => {
      readFile(data => {
        const key = req.params['id'];
        data[key].musique = req.body.musique;
    
        writeFile(JSON.stringify(data, null, 2), () => {
          res.status(200).send('new user added');
        });
      }, true);
    });

    //routes pour les likes et les checks
    app.post('/users_like/:id', (req, res) => {
      readFile(data => {
        const key = req.params['id'];
        const uti = req.body.likeIn;
        var d = new Date();
        var date = d.getDate() + '\\' +(d.getMonth()+1) + '\\' + d.getFullYear();
        var heure = d.getHours() + ':' + d.getMinutes();
        const obj= {id : data[uti].email,firstname:data[uti].firstname, name : data[uti].name, time: date,heure:heure, Notif : "Liké"} 
        if(!( data[key].likeOut.includes(req.body.likeOut)) &&
            !( data[req.body.likeOut].likeIn.includes(req.body.likeIn))&&(data[key].likeNumbers<5)
        ){
          data[key].likeNumbers++
          if (!( data[key].likeOut.includes(req.body.likeOut) )) {
            data[key].likeOut = data[key].likeOut.concat(req.body.likeOut);
          }
          if (!( data[req.body.likeOut].likeIn.includes(req.body.likeIn) )) {
            data[req.body.likeOut].likeIn = data[req.body.likeOut].likeIn.concat(req.body.likeIn);
          }
          if (!( data[req.body.likeOut].likeInNotif.includes(obj) )) {
            data[req.body.likeOut].likeInNotif = data[req.body.likeOut].likeInNotif.concat(obj);
          }
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send('user liked');
          });
        }else{
          res.status(409).send('user Déja liké');
        }
        
      }, true);
    });

    app.post('/users_check/:id', (req, res) => {
      readFile(data => {
        const key = req.params['id'];
        const uti = req.body.checkIn;
        var d= new Date();
        var date = d.getDate() + '\\' +(d.getMonth()+1) + '\\' + d.getFullYear();
        var heure = d.getHours() + ':' + d.getMinutes();
        const obj= {id : data[uti].email,firstname:data[uti].firstname, name : data[uti].name, time: date,heure:heure, Notif: "Checké"}
        if ( !( data[key].checkOut.includes(req.body.checkOut) ) &&
              !( data[req.body.checkOut].checkIn.includes(req.body.checkIn))
        ){
          if (!( data[key].checkOut.includes(req.body.checkOut) )) {
            data[key].checkOut = data[key].checkOut.concat(req.body.checkOut);
          }
          if (!( data[req.body.checkOut].checkIn.includes(req.body.checkIn))) {
            data[req.body.checkOut].checkIn = data[req.body.checkOut].checkIn.concat(req.body.checkIn);
          }
          if (!( data[req.body.checkOut].likeInNotif.includes(obj.id))) {
            data[req.body.checkOut].likeInNotif = data[req.body.checkOut].likeInNotif.concat(obj);
          }
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send('user checked');
          });

        }else{
          res.status(409).send('user Déja checké');
        }
      }, true);
    });

    app.post('/masquer/:id', (req, res) => {
      readFile(data => {
        const key = req.params['id'];
        data[key].masqued = data[key].masqued.concat(req.body.masque)
          
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send('user checked');
          });

        }, true);
    });

    app.post('/signaler/:id', (req, res) => {
      const key = req.params['id'];
      let read =fs.readFileSync("./data/signalements.json",'utf8');
      signalements=JSON.parse(read);
      signalements.push({
        signaleur: key,
        signaled: req.body.signaled
      })
      
      fs.writeFile("./data/signalements.json", JSON.stringify(signalements), () => {res.status(200).send('user signlaed')})
    });
      
    //routes pour les matchs check et likeIn
    app.post('/users_checkM/:id', (req, res) => {
      readFile(data => {
        const key = req.params['id'];
        if (!(data[key].matchLike.includes(req.body.match))){
          data[key].matchLike = data[key].matchLike.concat({email:req.body.match,key:req.body.match +key});
          data[key].likeInNotif.forEach(obj=>{
            if(obj.id === req.body.match){
              data[key].likeInNotif = data[key].likeInNotif.filter(item => item !== obj)
            }
          })
        }
        if (!(data[req.body.match].matchLike.includes(key))){
          data[req.body.match].matchLike = data[req.body.match].matchLike.concat({email:key,key:req.body.match +key});
          data[req.body.match].likeInNotif.forEach(obj=>{
            if(obj.id === key){
              data[req.body.match].likeInNotif = data[req.body.match].likeInNotif.filter(item => item !== obj)
            }
          })
        }
        writeFile(JSON.stringify(data, null, 2), () => {
          res.status(200).send('match');
        });
      }, true);
    });

    app.post('/users_checkM/:id', (req, res) => {
      readFile(data => {
        const key = req.params['id'];
        if(!(data[key].matchCheck.includes(req.body.match))){
          data[key].matchCheck = data[key].matchCheck.concat(req.body.match);
        }
        if(!(data[req.body.match].matchCheck.includes(key))){
          data[req.body.match].matchCheck = data[req.body.match].matchCheck.concat(key);
        }
        writeFile(JSON.stringify(data, null, 2), () => {
          res.status(200).send('match');
        });
      }, true);
    });

    app.post('/raz', (req, res) => {
      readFile(data => {
        for (let user of Object.keys(data)){
          data[user].likeOut = req.body.zero;
          data[user].checkOut = req.body.zero;
          data[user].likeIn = req.body.zero;
          data[user].checkIn = req.body.zero;
          data[user].matchLike = req.body.zero;
          data[user].matchCheck = req.body.zero;
          data[user].likeNumbers = 0;
          data[user].masqued = [];

        }
        writeFile(JSON.stringify(data, null, 2), () => {
          res.status(200).send('match');
        });
      }, true);
    });


    app.post('/getusername',(req,res)=> {
      let read =fs.readFileSync(dataPath,'utf8');
      let objread=JSON.parse(read);
      let profilToSend=[];
      
      for( let user in objread){
        if(String(objread[user].name).includes(req.body.name)||String(objread[user].firstname).includes(req.body.name)){ 
          profilToSend.push(objread[user]);
        }
      }
      res.status(200).send(profilToSend);
    })

    function creatUser(email,pdp,name,firstname){
      return {
        email : email,
        pdp: pdp,
        name:name,
        firstname:firstname
      }
    }

    app.post('/getcourprofil',(req,res)=> {
      let read =fs.readFileSync(dataPath,'utf8');
      let readBis =fs.readFileSync(dataPathBis,'utf8');
      let objread=JSON.parse(read);
      let objreadbis=JSON.parse(readBis);
      let profilToSend=[];
      for (let user in objread){
        if(String(objread[user].name).includes(req.body.name)){
          for(let user in objreadbis){
            if (String(objread[user].email)==String(objreadbis[user].email)){
              profilToSend.push(creatUser(objread[user].email,objreadbis[user].pdp.uri,objread[user].name,objread[user].firstname)) 
            }
          }
        }
      }
      res.status(200).send(profilToSend);
    })
    app.get('/transaction/:id',(req,res)=>{
      readFile(data => {
        dataReadbis=JSON.parse(fs.readFileSync(dataPathBis,'utf8'))
        const key = req.params['id'];
        let result=[]
        data[key].matchCheck.forEach(element => {
          if(dataReadbis[element.email].pdp==undefined){
            result.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:"",
              maPdp:(dataReadbis[key].pdp.length==0) ? "" : dataReadbis[key].pdp[0].uri
            })
          }else{
            result.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:dataReadbis[element.email].pdp,
              maPdp:(dataReadbis[key].pdp ==undefined) ? "" : dataReadbis[key].pdp[0].uri
             
            });
          }
        });

        data[key].matchLike.forEach(element => {
          if(dataReadbis[element.email].pdp.length==0){
            result.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:"",
              maPdp:(dataReadbis[key].pdp.length==0) ? "" : dataReadbis[key].pdp[0].uri
             
            })
          }else{
            result.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:dataReadbis[element.email].pdp,
              maPdp:(dataReadbis[key].pdp.length==0) ? "" : dataReadbis[key].pdp[0].uri
              
            })
          }
        });
        res.status(200).send(result)
      }, true);
    })
    app.get('/nameFirstname/:id',(req,res)=>{
      readFile(data => {
        dataReadbis=JSON.parse(fs.readFileSync(dataPathBis,'utf8'))
        const key = req.params['id'];
        let result = {
          check : [],
          like : [],
          
        };

        data[key].matchCheck.forEach(element => {
          console.log(element)
          if(dataReadbis[element.email].pdp==undefined){
            result.check.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:"",
              maPdp:(dataReadbis[key].pdp.length==0) ? "" : dataReadbis[key].pdp[0].uri
            })
          }else{
            result.check.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:dataReadbis[element.email].pdp,
              maPdp:(dataReadbis[key].pdp ==undefined) ? "" : dataReadbis[key].pdp[0].uri
             
            });
          }
        });

        data[key].matchLike.forEach(element => {
          if(dataReadbis[element.email].pdp.length==0){
            result.like.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:"",
              maPdp:(dataReadbis[key].pdp.length==0) ? "" : dataReadbis[key].pdp[0].uri
             
            })
          }else{
            result.like.push({
              email : element.email,
              name : data[element.email].name.concat(" "+data[element.email].firstname),
              pdp:dataReadbis[element.email].pdp,
              maPdp:(dataReadbis[key].pdp.length==0) ? "" : dataReadbis[key].pdp[0].uri
              
            })
          }
        });
        res.status(200).send(result)
      }, true);
    })

    app.post('/idOfPersonWeWantToTalk/:id',(req,res)=>{
      let keyPrivateToTalk
      EmailOfPersonConnect=req.params.id  
      EmailOfPersonWeWantToTalk=req.body.email
      readFile(data => {
        data[EmailOfPersonConnect].matchCheck.forEach(element => {
          if(element.key.includes(EmailOfPersonConnect)&&element.key.includes(EmailOfPersonWeWantToTalk)){
            
            keyPrivateToTalk=element.key
            res.status(200).send(keyPrivateToTalk)
          }
        });
        data[EmailOfPersonConnect].matchLike.forEach(element => {
          if(element.key.includes(EmailOfPersonConnect)&&element.key.includes(EmailOfPersonWeWantToTalk)){
            
            keyPrivateToTalk=element.key
            res.status(200).send(keyPrivateToTalk)
          }
        });
        
       
        io.on("connection", (socket) => {
          socket.join(keyPrivateToTalk)
          userIds[socket.id] = currentUserId++; 
          console.log(userIds)  
          console.log(keyPrivateToTalk)
          messageHandler.handleMessage(socket, userIds, keyPrivateToTalk);
        });   
      },true)
    })

    app.get('/Ucoin/:id',(req,res) =>{
      email=req.params.id
      console.log(email)
      readFile(data => {
        ucoin=data[email].ucoin
        res.status(200).json({
          ucoin:ucoin
        })
      },true)
    })
}



async function sendResetPass(email){
  const mailTransport= nodemailer.createTransport({
      service :"Gmail", //"SendPulse", no need to set host or port etc     
      port: 587,
      secure: false,
      auth : {
          user: "unizz.service@gmail.com",  
          pass:"ProjetL2S2021",
      }
  })
  const message = await mailTransport.sendMail({
      from: '"UNIZZ"<unizz.service@gmail.com>',
      to: email,
      subject: "reset pass-word",
      text: "Bonjour votre mot de passe à été changé. Bonne journée :)",
       
  })
  
}
    

module.exports = userRoutes;