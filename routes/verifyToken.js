const jwt = require('jsonwebtoken');
/*
const verifyJWT = (req, res, next) => {
    const token = req.headers["auth-token"];

    if (!token){ 
      res.status(401).send('Access Denied')
    }
    try {
        const verified =  jwt.verify(token, "accessTokenSecret");
        req.user = verified;
        next();
    }catch(err){
        res.status(400).send('Invalid Token');
    }
      
    
};*/

const verifyJWT = (req, res, next) => {
    const token = req.headers["x-access-token"];
  
    if (!token){ 
      res.send("We need a token");
    }else{
      jwt.verify(token, "accessTokenSecret", (err, decoded)=>{
        if (err){
          res.json({auth: false, message:"Authentification failed"});
        }else{
          req.userId = decoded.username;
          next();
        }
      });
    }
  };

  

module.exports = verifyJWT;