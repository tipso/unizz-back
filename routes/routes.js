

const userRoutes = require('./users');
const user_attRoutes = require('./users_att');
const coursRoute = require('./cours');
const appRouter = (app, fs) => {
  
  app.get('/', (req, res) => {
    res.send("This is a message");
  });
  userRoutes(app, fs);
  user_attRoutes(app, fs);
  coursRoute(app, fs);
  
};

module.exports = appRouter;
