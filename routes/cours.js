

const coursRoute= (app, fs) => {
    const dataPath='./data/cours.json'
    const readFile = (
        callback,
        returnJson = false,
        filePath = dataPath,
        encoding = 'utf8'
      ) => {
        fs.readFile(filePath, encoding, (err, data) => {
          if (err) {
            throw err;
          }
    
          callback(returnJson ? JSON.parse(data) : data);
        });
      };
      const readFileUser_att = (
        callback,
        returnJson = false,
        filePath = './data/users_att.json',
        encoding = 'utf8'
      ) => {
        fs.readFile(filePath, encoding, (err, data) => {
          if (err) {
            throw err;
          }
    
          callback(returnJson ? JSON.parse(data) : data);
        });
      };
      const readFileUser = (
        callback,
        returnJson = false,
        filePath = './data/users.json',
        encoding = 'utf8'
      ) => {
        fs.readFile(filePath, encoding, (err, data) => {
          if (err) {
            throw err;
          }
    
          callback(returnJson ? JSON.parse(data) : data);
        });
      };
    
    const writeFile = (
          fileData,
          callback,
          filePath = dataPath,
          encoding = 'utf8'
        ) => {
          fs.writeFile(filePath, fileData, encoding, err => {
            if (err) {
              throw err;
            }
            callback();
          });
        };
        const writeFileUser = (
          fileData,
          callback,
          filePath = './data/users.json',
          encoding = 'utf8'
        ) => {
          fs.writeFile(filePath, fileData, encoding, err => {
            if (err) {
              throw err;
            }
      
            callback();
          });
        };
      recupNameAndFirstname=(email)=>{
        readFileUser(data=>{
          let nameAndFirstName=data[email].firstname+" "+data[email].name
          return nameAndFirstName
        },true)
      }
        
        app.post('/demandeCours/:id',(req,res)=>{
            readFile(data =>{
              email=req.params.id
              let {cours,topic,demande,ucoins}=req.body

              data[cours].push({
                topic:topic,
                demande:demande,
                user:email,
                key:cours,
                ucoin:ucoins
              })
              writeFile(JSON.stringify(data, null, 2), () => {
                res.status(200).send('demande cours envoyee');
              });
            },true);
        })
        app.get('/historique/:id',(req,res)=>{
          let id = req.params.id
          readFileUser(data=>{
            let historique=data[id].historique
            if(historique){
              readFileUser_att(data1=>{
                historique.forEach(element=>{
                  element['pdp']=data1[element.to].pdp
                  element.to= data[element.to].name.concat(" " +data[element.to].firstname)
                })
                res.status(200).send(historique)
              },true)
            }
            else{
              res.status(400).send('error to read file!')
            }
          },true)
        })
        app.post('/giveUcoin/:id',(req, res)=>{
          let id = req.params.id
          let person=req.body.to
          let montant=req.body.coins
          readFileUser(data => {
            if(data[id].ucoin-montant <0){
              res.status(400).send('compte à decouvert');
            }else{
              data[id].ucoin=parseInt(data[id].ucoin)-parseInt(montant)
              data[person].ucoin=parseInt(data[person].ucoin)-parseInt(montant)
              console.log(typeof(data[id].historique))
              data[id].historique=data[id].historique.concat({"to":person,"montant":montant})
              writeFileUser(JSON.stringify(data, null, 2), () => {
                res.status(200).send('transaction  saved!');
              });
            }
          },true)
          
        })
        app.post('/coursHelp',(req,res)=>{
          let {matiere,level}=req.body
          console.log(matiere)
          let convertionMatiereReceived={
            sciences_sociales:"Sciences sociales",
            science_vie_terre:"Sciences de la vie et de la Terre",
            economie_gestion:"Economie / Gestion",
            staps:"STAPS",
            science_pour_la_sante:"Sciences pour la santé",
            mathematiques:"Mathématiques",
            informatique:"Informatique",
          }
          let convertionLevelReceived={
            L1:"L1",
            L2:"L2",
            L3:"L3",
            M1:"M1",
            M2:"M2",
          }
          readFile(data=>{
            let tabToSend=[]
            
            let matiereToSend=[]
            let levelToSend=[]
            Object.keys(matiere).forEach(key=>{
              if(matiere[key]){
                matiereToSend.push(convertionMatiereReceived[key])
              }
            })
            Object.keys(level).forEach(key=>{
              if(level[key]){
                levelToSend.push(convertionLevelReceived[key])
              }
            })
            if(matiereToSend.length==0  && levelToSend.length==0){
              Object.keys(data).forEach(element=>{
              
                console.log(element)
                if(data[element].length !=0){
                  
                  data[element].forEach(value=>{
                    var pdp
                    
                    dataPDP=JSON.parse(fs.readFileSync('./data/users_att.json','utf8'))
                      pdp=dataPDP[value.user].pdp
                      
                      value.pdp = (pdp.length==0)? 'https://www.blog-nouvelles-technologies.fr/wp-content/uploads/2017/12/detective-avatar-icon-01-.jpg':pdp
                      
                    
                    dataName=JSON.parse(fs.readFileSync('./data/users.json','utf8'))
                      
                      let name=dataName[value.user].firstname + " " + dataName[value.user].name
                      value.name=name
                    
                    tabToSend.push(value)
                  })
  
                }
                
              })
            }else if(matiereToSend.length==0  && levelToSend.length!=0){
              Object.keys(data).forEach(element=>{
                levelToSend.forEach(itemselect=>{
                  if(data[element].length !=0 && element.includes(convertionLevelReceived[itemselect] )){
                  
                    data[element].forEach(value=>{
                      var pdp
                      
                      dataPDP=JSON.parse(fs.readFileSync('./data/users_att.json','utf8'))
                        pdp=dataPDP[value.user].pdp
                        
                        value.pdp = (pdp.length==0)? 'https://www.blog-nouvelles-technologies.fr/wp-content/uploads/2017/12/detective-avatar-icon-01-.jpg':pdp
                        
                      
                      dataName=JSON.parse(fs.readFileSync('./data/users.json','utf8'))
                        
                        let name=dataName[value.user].firstname + " " + dataName[value.user].name
                        value.name=name
                      
                      tabToSend.push(value)
                    })
    
                  }
                })
              })
            }else if(matiereToSend.length!=0  && levelToSend.length==0){
              Object.keys(data).forEach(element=>{
                matiereToSend.forEach(itemselect=>{
                  
                  if(data[element].length !=0 && element.includes(itemselect )){
                    data[element].forEach(value=>{
                      console.log(value)
                      var pdp
                      
                      dataPDP=JSON.parse(fs.readFileSync('./data/users_att.json','utf8'))
                        pdp=dataPDP[value.user].pdp
                        
                        value.pdp = (pdp.length==0)? 'https://www.blog-nouvelles-technologies.fr/wp-content/uploads/2017/12/detective-avatar-icon-01-.jpg':pdp
                        
                      
                      dataName=JSON.parse(fs.readFileSync('./data/users.json','utf8'))
                        
                        let name=dataName[value.user].firstname + " " + dataName[value.user].name
                        value.name=name
                      
                      tabToSend.push(value)
                      
                    })
    
                  }
                })
              })
            }else{
              Object.keys(data).forEach(element=>{
                matiereToSend.forEach(matiereItem=>{
                  levelToSend.forEach(levelItem=>{
                    if(element.includes(matiereItem) && element.includes(levelItem)){
                      data[element].forEach(value=>{
                        var pdp
                        
                        dataPDP=JSON.parse(fs.readFileSync('./data/users_att.json','utf8'))
                          pdp=dataPDP[value.user].pdp
                          
                          value.pdp = (pdp.length==0)? 'https://www.blog-nouvelles-technologies.fr/wp-content/uploads/2017/12/detective-avatar-icon-01-.jpg':pdp
                          
                        
                        dataName=JSON.parse(fs.readFileSync('./data/users.json','utf8'))
                          
                          let name=dataName[value.user].firstname + " " + dataName[value.user].name
                          value.name=name
                        
                        tabToSend.push(value)
                      })
                    }
                  })
                })
              })
            }
            
            console.log(tabToSend)
          res.status(200).send(tabToSend)
          },true)
        })
        app.post('/getcourprofil',(req,res)=> {
            let read =fs.readFileSync(dataPath,'utf8');
            let readBis =fs.readFileSync(dataPathBis,'utf8');
            let objread=JSON.parse(read);
            let objreadbis=JSON.parse(readBis);
            let profilToSend=[];
            console.log(String(objread[user].name).includes(req.body.name));
            for (let user in objread){
              if(String(objread[user].name).includes(req.body.name)){
                for(let user in objreadbis){
                  if (String(objread[user].email)==String(objreadbis[user].email)){
                    profilToSend.push(creatUser(objread[user].email,objreadbis[user].pdp.uri,objread[user].name,objread[user].firstname)) 
                  }
                }
              }
            }
            console.log(profilToSend)
            res.status(200).send(profilToSend);
          })
        
}
module.exports=coursRoute

