const verifyJWT = require('./verifyToken');
const user_attRoutes = (app, fs) => {

    const dataPath = './data/users_att.json';

    const readFile = (
      callback,
      returnJson = false,
      filePath = dataPath,
      encoding = 'utf8'
    ) => {
      fs.readFile(filePath, encoding, (err, data) => {
        if (err) {
          throw err;
        }
  
        callback(returnJson ? JSON.parse(data) : data);
      });
    };

    const writeFile = (
        fileData,
        callback,
        filePath = dataPath,
        encoding = 'utf8'
      ) => {
        fs.writeFile(filePath, fileData, encoding, err => {
          if (err) {
            throw err;
          }
    
          callback();
        });
      };

      app.get('/users_att', (req, res) => {
        readFile(data => {
          res.send(data);
        }, true);
      });

      app.post('/users_att/:id', (req, res) => {
        readFile(data => {
          const key = req.params['id'];
          data[key] = req.body;
      
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send('new user added');
          });
        }, true);
      });
// Pour Spotify
      app.post('/users_att2/:id', (req, res) => {
        readFile(data => {
          const key = req.params['id'];
          data[key].musique = req.body.musique;
      
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send('new user added');
          });
        }, true);
      });
      
      app.post('/images2/:id', (req, res) => {
        readFile(data => {
          const key = req.params['id'];
          data[key].picture = data[key].picture.concat(req.body.imgsource);
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send(data[key].picture);
          });
        }, true);
      });

      app.post('/images3/:id', (req, res) => {
        readFile(data => {
          const key = req.params['id'];
          data[key].picture = data[key].picture.concat(req.body.imgsource);
          data[key].pdp.pop();
          data[key].pdp = data[key].pdp.concat(req.body.imgsource);
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send(data[key].pdp);
          });
        }, true);
      });
     

      app.get('/images1/:id', (req, res) => {
        readFile(data => {
          const key = req.params['id'];
          let picture = data[key].picture;
          res.status(200).send(picture);
        },true);
        
      })

      app.get('/pdp/:id', (req, res) => {
        readFile(data => {
          const key = req.params['id'];
          let pdp = data[key].pdp;
          res.status(200).send(pdp);
          
        },true);
        
      })

      app.post('/zero_photo/:id', (req, res) => {
        readFile(data => {
          const key = req.params['id'];
          data[key].picture = []
          
          writeFile(JSON.stringify(data, null, 2), () => {
            res.status(200).send('match');
          });
        }, true);
      });

      app.get('/photoprofil/:id'),(req,res)=>{
        readFile(data => {
          const key = req.params['id'];
          let photoprofil=data[key].pdp
          res.send(200).send(photoprofil)
        }, true);
        
      }

}


module.exports = user_attRoutes;